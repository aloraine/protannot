### Introduction

ProtAnnot is an IGB App that displays protein annotations next to gene models, making it easy to see how alternative splicing, alternative promoters, and alternative polyadenylation changes gene function.

ProtAnnot is developed in the Loraine Lab, part of the Bioinformatics and Genomics Department at University of North Carolina at Charlotte. 

### Install ProtAnnot

Use the [IGB App Store](https://apps.bioviz.org) or the IGB App Manager to install ProtAnnot into IGB.

### Start ProtAnnot from IGB

Once ProtAnnot is installed, you can open ProtAnnot in IGB by selecting one or more gene models on the same strand of DNA and selecting **Tools > Start ProtAnnot**. 

A new window will then open, showing the same gene models you selected. 

### Using ProtAnnot

Gene models will look the same as in IGB, except that exons will be color-coding according to their frame of translation. 
Also, ProtAnnot numbers the base positions starting from the beginning of the gene, not the chromosome. However, ProtAnnot shows the same
reference sequence as IGB.

In addition to the gene models, ProtAnnot shows an exon "summary" graphic at the bottom of the display that summarizes the number of gene models that have sequence at each position along the axis.
(It's a bit like a coverage graph, but for gene models.)

In ProtAnnot, you can zoom and scroll the same as in IGB. 

To run a search, click the Interpro tab and go from there.

Once a search finishes, ProtAnnot will display the results. ProtAnnot will also display a link to the XML output files from InterPro. 

You can save the results. Just use the **File** menu to save your data to a file. Later on, you can open the file without needing to select gene models in the main IGB window.

### Further info

For more information about ProtAnnot, including an example gene with an interesting alternative splicing pattern, see the ProtAnnot paper:

[ProtAnnot: an App for Integrated Genome Browser to display how alternative splicing and transcription affect proteins](https://www.ncbi.nlm.nih.gov/pubmed/27153567)
